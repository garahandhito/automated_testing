from enum import Enum


class CommonDict(dict):

    def __getattr__(self, item):
        if item in self:
            return self[item]
        raise AttributeError(item)

    ENVIRONMENTS = {'blueranger': 'http://bluerangers1.ap-southeast-1.elasticbeanstalk.com/',
                    'PPE': 'http://ppe-jurnal-1.ap-southeast-1.elasticbeanstalk.com',
                    'jurnal-ledger': 'http://jurnal.ap-southeast-1.elasticbeanstalk.com/',
                    'jurnal-master': 'http://jurnal-master.6qsnunpvng.ap-southeast-1.elasticbeanstalk.com'}


class CommonLocators(Enum):

    EMAIL_LOGIN = 'user_email'
    PASSWORD_LOGIN = 'user_password'
    LOGIN_BUTTON = 'button[type="submit"]'

    DASHBOARD = 'vnav-dashboard-link'
    NAV_BAR = 'nav.navbar.sm.top-navbar'

    BACKDROP = 'div.dropdown-backdrop'
    # ERROR500_A = 'div.col-xs-12 error500-icon'
    # ERROR500_B = '//div[contains(text(), "HTTP ERROR 500")'
    # BUTTON404 = 'a.btn.btn-404'

    PAGE = 'modj'
    ERROR_500 = 'modj_errors'
    HANDLE_ERROR_500 = '//figure[@class="logo-small"]'

    CONFIRM_SA_WARNING = 'confirm_stock_adjustment_warning'
    YES = 'yes'

    PERSON = 's2id_transaction_person_id'
    SHIPPING = 'transaction_is_shipped'
    SHIPPING_FEE = 'transaction_shipping_price'
    DATE = 'transaction_transaction_date'
    SELECTED_DATE = 'td.active.day'
    TAGS = ['s2id_transaction_tag_ids', 's2id_asset_tag_ids']
    CURRENCY_LIST = 's2id_transaction_currency_list_id'
    EDIT_CURRENCY = 'edit_currency_href'
    CUSTOM_CURRENCY = 'custom_currency_field'
    SUBMIT_RATE = 'button.btn.btn-success.edit_multi_currency'
    PRODUCT_LINE = ['s2id_transaction_transaction_lines_attributes_0_product_id',
                    's2id_transaction_transaction_lines_attributes_1_product_id']
    QTY_LINE = ['transaction_transaction_lines_attributes_0_quantity',
                'transaction_transaction_lines_attributes_1_quantity']
    UNIT_LINE = ['transaction_transaction_lines_attributes_0_unit_id',
                 'transaction_transaction_lines_attributes_1_unit_id']
    PURCHASE_RETURN_QTY_LINE = ['purchase_return_transaction_return_lines_attributes_0_return_quantity',
                                'purchase_return_transaction_return_lines_attributes_1_return_quantity']
    SALES_RETURN_QTY_LINE = ['sales_return_transaction_return_lines_attributes_0_return_quantity',
                             'sales_return_transaction_return_lines_attributes_1_return_quantity']
    PRICE_LINE = ['transaction_transaction_lines_attributes_0_rate',
                  'transaction_transaction_lines_attributes_1_rate']
    DISCOUNT_LINE = ['transaction_transaction_lines_attributes_0_discount',
                     'transaction_transaction_lines_attributes_1_discount']
    TAX_LINE = ['s2id_transaction_transaction_lines_attributes_0_line_tax_id',
                's2id_transaction_transaction_lines_attributes_1_line_tax_id']
    SUBTOTAL_LINE = ['transaction_transaction_lines_attributes_0_amount',
                     'transaction_transaction_lines_attributes_1_amount']
    WITHOLDING_TOGGLE = 'witholding_toggle'
    WITHOLDING_CURRENCY = 'button.btn.btn-default.btn-switch.btn-toggle-percent-values.toggle-currency-symbol:nth-child(2)'
    WITHOLDING_AMOUNT = 'transaction[witholding_value]'
    WITHOLDING_ACCOUNT = 's2id_witholding_account'
    DEPOSIT_AMOUNT = 'transaction_deposit'
    DEPOSIT_ACCOUNT = 's2id_invoice_deposit_to'
    CREATE_BUTTON = ['create_button', '//button[contains(text(), "Create")]']
    SUBMIT_PAYMENT = '//button[contains(text(), "Payment")]'
    SUBMIT_EDIT = 'update_invoice'

    TAGS_ANCHOR = 'label.control-label'
    ASSET_TAG_ANCHOR = '//div[contains(text(), "Tags")]'
    PRICE_ANCHOR = 'th.price-line'
    WITHOLDING_ANCHOR = 'witholding-amount'
    DISCOUNT_LINE_ANCHOR = 'th.discount-line'
    DROPUP_BUTTON = 'div.btn-group.dropup.print-preview:nth-child(2)'
    ORDER_DROPUP = 'span.fa.fa-bars.fa-1x'
    DROPUP_ACTIONS = 'span.fa.fa-bars.fa-1x'

    DATA_VALUE = 'data-value'
