from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException, TimeoutException
from common.Utility import Utility, UtilityLocators
from common.CommonLocators import CommonDict, CommonLocators
from common.TransactionLocators import TransactionDict, TransactionLocators
from datetime import datetime
from random import randint
import time


class GeneralFunctions(Utility):

    def __init__(self, driver):
        Utility.__init__(self, driver)
        self.driver = driver

    def open_environment(self, environment):
        environment_url = CommonDict.ENVIRONMENTS[environment]
        self.driver.get(environment_url)
        print('Running test case in ' + environment)
        print(' ')

    def end_test(self):
        print('Test cases completed at ' + str(datetime.now().isoformat(' ', 'seconds')))
        self.driver.quit()

    def login_as(self, email, password):
        email_field = self.driver.find_element_by_id(CommonLocators.EMAIL_LOGIN.value)
        super(GeneralFunctions, self).input_item(email_field, email)
        password_field = self.driver.find_element_by_id(CommonLocators.PASSWORD_LOGIN.value)
        super(GeneralFunctions, self).input_item(password_field, password)
        sign_in = self.driver.find_element_by_css_selector(CommonLocators.LOGIN_BUTTON.value)
        super(GeneralFunctions, self).select_item(sign_in)
        print('Logging in...')
        assert ('Dashboard' or 'Dasbor' in self.driver.title), "Failed to login"
        print('Logged in successfully')
        print(' ')

    def check_error_500(self):
        try:
            self.driver.find_element_by_id(CommonLocators.ERROR_500.value)
            return True
        except NoSuchElementException:
            return False

    def handle_error_500(self):
        print(self.driver.current_url)
        print('!!! ERROR 500 !!!')
        try:
            handle_error = self.driver.find_element_by_xpath(CommonLocators.HANDLE_ERROR_500.value)
            super(GeneralFunctions, self).select_item(handle_error)
        except NoSuchElementException:
            self.driver.back()
            try:
                self.driver.find_element_by_css_selector(CommonLocators.NAV_BAR.value)
            except NoSuchElementException:
                self.driver.back()
                pass

    @staticmethod
    def documentation(documentation_name):
        print('\nDocumentation: ' + documentation_name)

    @staticmethod
    def test_case(test_case_name):
        print('Test case: ' + test_case_name)

    def view_journal_entry(self):
        self.driver.refresh()
        try:
            try:
                self.driver.find_element_by_css_selector(TransactionLocators.CHECK_JOURNAL_ENTRY.value[0]).click()
            except NoSuchElementException:
                self.driver.find_element_by_css_selector(TransactionLocators.CHECK_JOURNAL_ENTRY.value[1]).click()
            WebDriverWait(self.driver, 5).until(
                ec.visibility_of_element_located((By.CSS_SELECTOR, TransactionLocators.JOURNAL_ENTRY_TABLE.value))
                )
        except NoSuchElementException:
            print('Failed to create transaction')
            raise ValueError

    def check_unrealized_realized(self, is_gl='yes'):
        if is_gl == 'yes':
            debit_selector = TransactionLocators.UN_REALIZED.value[2]
            credit_selector = TransactionLocators.UN_REALIZED.value[3]
        else:
            debit_selector = TransactionLocators.UN_REALIZED.value[0]
            credit_selector = TransactionLocators.UN_REALIZED.value[1]
        try:
            unrealized_debit = self.driver.find_element_by_css_selector(debit_selector).get_attribute('innerHTML')
            unrealized_credit = self.driver.find_element_by_css_selector(credit_selector).get_attribute('innerHTML')
            print('Unrealized/realized debit is ' + unrealized_debit)
            print('Unrealized/realized credit is ' + unrealized_credit)
        except NoSuchElementException:
            pass

    def check_journal_entry(self):
        self.view_journal_entry()
        try:
            debit = self.driver.find_element_by_css_selector(
                TransactionLocators.JOURNAL_ENTRY.value[0]).get_attribute('innerHTML')
            credit = self.driver.find_element_by_css_selector(
                TransactionLocators.JOURNAL_ENTRY.value[1]).get_attribute('innerHTML')
        except NoSuchElementException:
            debit = self.driver.find_element_by_css_selector(
                TransactionLocators.JOURNAL_ENTRY.value[2]).get_attribute('innerHTML')
            credit = self.driver.find_element_by_css_selector(
                TransactionLocators.JOURNAL_ENTRY.value[3]).get_attribute('innerHTML')
        print('Checking journal entry...')
        index = 0
        while (debit != credit or debit == '0,00' or credit == '0,00') and index < 2:
            self.view_journal_entry()
            index += 1
        assert((debit != '0,00') and (credit != '0,00')), "Debit or credit is 0, please check sidekiq"
        assert (debit == credit), "Journal entry is not balanced"
        print('Journal entry is balanced')
        self.driver.refresh()

    def view_transaction_tab(self, transaction_type):  # invoice, order, quote, delivery
        if transaction_type == 'invoice':
            tab = TransactionLocators.TRANSACTION_TAB.value[0]
        elif transaction_type == 'order':
            tab = TransactionLocators.TRANSACTION_TAB.value[1]
        elif transaction_type == 'quote':
            tab = TransactionLocators.TRANSACTION_TAB.value[2]
        else:
            tab = TransactionLocators.TRANSACTION_TAB.value[3]
        self.driver.find_element_by_id(tab).click()

    def new_transaction(self, transaction_type):
        self.driver.find_element_by_id(CommonLocators.DASHBOARD.value).click()
        current_url = self.driver.current_url
        transaction = TransactionDict.CREATE_TRANSACTION_TYPE[transaction_type]
        new_transaction_url = current_url + transaction + 'new'
        self.driver.get(new_transaction_url)
        print('Creating new ' + transaction_type + '...')

    def select_person(self, person):
        super(GeneralFunctions, self).select2_search_and_select(CommonLocators.PERSON.value, person)

    def add_delivery_shipping_fee(self, fee):
        shipping_fee_field = self.driver.find_element_by_id(CommonLocators.SHIPPING_FEE.value)
        self.input_item(shipping_fee_field, fee)

    def add_shipping(self, shipping_fee=''):
        super(GeneralFunctions, self).scroll_to('top')
        shipping_checkbox = self.driver.find_element_by_id(CommonLocators.SHIPPING.value)
        super(GeneralFunctions, self).select_item(shipping_checkbox)
        if shipping_fee != '':
            super(GeneralFunctions, self).scroll_to('bottom')
            self.add_delivery_shipping_fee(shipping_fee)
        else:
            pass

    def set_transaction_date(self, date, is_return='', transaction_type=''):  # for return, is_return='yes'
        if is_return == 'yes':
            if transaction_type == 'purchase':
                date_field_id = TransactionLocators.RETURN.value[0]
            else:
                date_field_id = TransactionLocators.RETURN.value[1]
        else:
            date_field_id = CommonLocators.DATE.value
        date_field = self.driver.find_element_by_id(date_field_id)
        self.input_item(date_field, date)
        self.driver.find_element_by_css_selector(CommonLocators.SELECTED_DATE.value).click()

    def random_transaction_date(self, year):
        day = randint(1, 28)
        month = randint(1, 12)
        date = str(day) + '/' + str(month) + '/' + year
        self. set_transaction_date(date)

    def add_tags(self, tags, is_asset='no'):
        for tag in tags:
            if is_asset == 'no':
                tags_field = self.driver.find_element_by_id(CommonLocators.TAGS.value[0])
            else:
                tags_field = self.driver.find_element_by_id(CommonLocators.TAGS.value[1])
            self.select_item(tags_field)
            self.select2_wait()
            tag_field = self.driver.find_element_by_css_selector(UtilityLocators.SELECT2_INPUT.value)
            tag_field.send_keys(tag)
            try:
                self.select2_wait()
            except TimeoutException:
                pass
            tag_field.send_keys(Keys.RETURN)
            if is_asset == 'no':
                self.driver.find_element_by_css_selector(CommonLocators.TAGS_ANCHOR.value).click()
            else:
                self.driver.find_element_by_xpath(CommonLocators.ASSET_TAG_ANCHOR.value).click()

    # noinspection PyBroadException
    def use_mc(self, currency, rate=''):
        try:
            anchor = self.driver.find_element_by_id(CommonLocators.PERSON.value)
            self.driver.execute_script("arguments[0].scrollIntoView();", anchor)
            mc_button = self.driver.find_element_by_id(CommonLocators.CURRENCY_LIST.value)
            super(GeneralFunctions, self).select_item(mc_button)
            time.sleep(0.5)
            currency_field = self.driver.find_element_by_css_selector(UtilityLocators.SELECT2_INPUT.value)
            super(GeneralFunctions, self).input_item(currency_field, currency)
            WebDriverWait(self.driver, 1).until(
                ec.element_to_be_clickable((By.CSS_SELECTOR, UtilityLocators.SEARCHED.value[1])))
        except Exception:
            pass
        if rate != '':
            WebDriverWait(self.driver, 1).until(
                ec.element_to_be_clickable((By.ID, CommonLocators.EDIT_CURRENCY.value)))
            self.driver.find_element_by_id(CommonLocators.EDIT_CURRENCY.value).click()
            WebDriverWait(self.driver, 3).until(
                ec.element_to_be_clickable((By.ID, CommonLocators.CUSTOM_CURRENCY.value)))
            custom_rate_field = self.driver.find_element_by_id(CommonLocators.CUSTOM_CURRENCY.value)
            super(GeneralFunctions, self).input_item(custom_rate_field, rate)
            self.driver.find_element_by_css_selector(CommonLocators.SUBMIT_RATE.value).click()
            WebDriverWait(self.driver, 1).until(
                ec.invisibility_of_element_located((By.CSS_SELECTOR, UtilityLocators.MODAL_BACKDROP.value)))
        else:
            pass

    def add_product(self, line, product_name):
        if line == 1:
            product_line_id = CommonLocators.PRODUCT_LINE.value[0]
        else:
            product_line_id = CommonLocators.PRODUCT_LINE.value[1]
        super(GeneralFunctions, self).select2_search_and_select(product_line_id, product_name)

    def set_product_qty(self, line, qty):
        if qty == 'random':
            qty = str(randint(1, 99))
        else:
            pass
        if line == 1:
            qty_line_id = CommonLocators.QTY_LINE.value[0]
        else:
            qty_line_id = CommonLocators.QTY_LINE.value[1]
        qty_field = self.driver.find_element_by_id(qty_line_id)
        if qty == 'random':
            qty = str(randint(1, 99))
        else:
            pass
        super(GeneralFunctions, self).input_item(qty_field, qty)

    def set_product_unit(self, line, unit):
        time.sleep(0.5)
        if line == 1:
            unit_line_id = CommonLocators.UNIT_LINE.value[0]
        else:
            unit_line_id = CommonLocators.UNIT_LINE.value[1]
        unit_line = self.driver.find_element_by_id(unit_line_id)
        super(GeneralFunctions, self).select_item(unit_line)
        time.sleep(0.5)
        xpath = '//select[@id="' + unit_line_id + '"]//option[contains(text(),"' + unit + '")]'
        selected_unit = self.driver.find_element_by_xpath(xpath)
        selected_unit.click()

    def set_return_product_qty(self, transaction_type, line, qty):  # transaction_type purchase/sales
        if transaction_type == 'purchase':
            if line == 1:
                return_qty_line_id = CommonLocators.PURCHASE_RETURN_QTY_LINE.value[0]
            else:
                return_qty_line_id = CommonLocators.PURCHASE_RETURN_QTY_LINE.value[1]
        else:
            if line == 1:
                return_qty_line_id = CommonLocators.SALES_RETURN_QTY_LINE.value[0]
            else:
                return_qty_line_id = CommonLocators.SALES_RETURN_QTY_LINE.value[1]
        return_qty_field = self.driver.find_element_by_id(return_qty_line_id)
        super(GeneralFunctions, self).input_item(return_qty_field, qty)

    def set_product_price(self, line, price):
        if price != '':
            if price == 'random':
                price = str(randint(1000, 100000))
            else:
                pass
            if line == 1:
                price_line_id = CommonLocators.PRICE_LINE.value[0]
            else:
                price_line_id = CommonLocators.PRICE_LINE.value[1]
            price_field = self.driver.find_element_by_id(price_line_id)
            super(GeneralFunctions, self).input_item(price_field, price)
            self.driver.find_element_by_css_selector('th.price-line').click()
        else:
            pass

    def add_line_discount(self, line, value):
        if line == 1:
            discount_line_id = CommonLocators.DISCOUNT_LINE.value[0]
        else:
            discount_line_id = CommonLocators.DISCOUNT_LINE.value[1]
        if value == 'random':
            value = str(randint(1, 99))
        else:
            pass
        discount_field = self.driver.find_element_by_id(discount_line_id)
        super(GeneralFunctions, self).input_item(discount_field, value)

    def add_tax(self, line, tax):
        if tax != '':
            if line == 1:
                tax_line_id = CommonLocators.TAX_LINE.value[0]
            else:
                tax_line_id = CommonLocators.TAX_LINE.value[1]
            super(GeneralFunctions, self).select2_search_and_select(tax_line_id, tax)
        else:
            pass

    def add_product_line(self, line, product_name, qty, unit='', price='', discount='', tax=''):
        self.add_product(line, product_name)
        self.set_product_qty(line, qty)
        self.set_product_unit(line, unit)
        self.set_product_price(line, price)
        self.add_line_discount(line, discount)
        self.add_tax(line, tax)

    def add_transaction_account(self, line, account):
        if line == 1:
            account_transaction_id = TransactionLocators.TRANSACTION_ACCOUNT.value[0]
        else:
            account_transaction_id = TransactionLocators.TRANSACTION_ACCOUNT.value[1]
        super(GeneralFunctions, self).select2_search_and_select(account_transaction_id, account)

    def set_account_amount(self, line, amount, amount_type='debit'):
        if line == 1:
            if amount_type == 'debit':
                account_amount_id = TransactionLocators.ACCOUNT_AMOUNT_DEBIT.value[0]
            else:
                account_amount_id = TransactionLocators.ACCOUNT_AMOUNT_CREDIT.value[0]
        else:
            if amount_type == 'debit':
                account_amount_id = TransactionLocators.ACCOUNT_AMOUNT_DEBIT[1]
            else:
                account_amount_id = TransactionLocators.ACCOUNT_AMOUNT_CREDIT[1]
        account_amount_field = self.driver.find_element_by_id(account_amount_id)
        super(GeneralFunctions, self).input_item(account_amount_field, amount)

    def insert_witholding_amount(self, amount, unit, account_number):
        self.driver.find_element_by_id(CommonLocators.WITHOLDING_TOGGLE.value).click()
        if unit != '%':
            self.driver.find_element_by_css_selector(CommonLocators.WITHOLDING_CURRENCY.value).click()
        else:
            pass
        witholding_amount_field = self.driver.find_element_by_name(CommonLocators.WITHOLDING_AMOUNT.value)
        self.input_item(witholding_amount_field, amount)
        self.driver.find_element_by_id(CommonLocators.WITHOLDING_ANCHOR.value).click()
        self.select2_search_and_select(CommonLocators.WITHOLDING_ACCOUNT.value, account_number)

    def add_deposit(self, amount, account_number):
        if amount == 'full':
            time.sleep(1)
            subtotal_line1 = self.driver.find_element_by_id(
                CommonLocators.SUBTOTAL_LINE.value[0]).get_attribute(CommonLocators.DATA_VALUE.value)
            subtotal_line2 = self.driver.find_element_by_id(
                CommonLocators.SUBTOTAL_LINE.value[1]).get_attribute(CommonLocators.DATA_VALUE.value)
            subtotal = float(subtotal_line1) + float(subtotal_line2)
            amount = str(subtotal)
        else:
            pass
        deposit_amount_field = self.driver.find_element_by_id(CommonLocators.DEPOSIT_AMOUNT.value)
        self.input_item(deposit_amount_field, amount)
        super(GeneralFunctions, self).scroll_to('bottom')
        self.select2_search_and_select(CommonLocators.DEPOSIT_ACCOUNT.value, account_number)

    def confirm_stock_adjustment_warning(self):
        try:
            WebDriverWait(self.driver, 1).until(ec.visibility_of_element_located((By.ID, CommonLocators.CONFIRM_SA_WARNING.value)))
            yes_button = self.driver.find_element_by_id(CommonLocators.YES.value)
            self.select_item(yes_button)
        except (NoSuchElementException, TimeoutException):
            pass

    def create_transaction(self, is_return='no'):
        super(GeneralFunctions, self).scroll_to('bottom')
        time.sleep(1)
        if is_return != 'no':
            create_button = self.driver.find_element_by_xpath(CommonLocators.CREATE_BUTTON.value[1])
        else:
            create_button = self.driver.find_element_by_id(CommonLocators.CREATE_BUTTON.value[0])
        self.select_item(create_button)
        self.confirm_stock_adjustment_warning()
        self.check_error_500()
        if self.check_error_500() is True:
            self.handle_error_500()
        else:
            print('Transaction created successfully')
            print(' ')
            pass

    def new_payment(self):
        actions = self.driver.find_element_by_css_selector(CommonLocators.DROPUP_BUTTON.value)
        self.select_item(actions)
        time.sleep(0.5)
        payment = self.driver.find_element_by_partial_link_text('Payment')
        self.select_item(payment)
        print('Creating new payment...')

    def create_payment(self):
        payment_button = self.driver.find_element_by_xpath(CommonLocators.SUBMIT_PAYMENT.value)
        self.select_item(payment_button)

    def new_delivery(self):
        order_dropup = self.driver.find_element_by_css_selector(CommonLocators.ORDER_DROPUP.value)
        super(GeneralFunctions, self).select_item(order_dropup)
        receive_delivery = self.driver.find_element_by_partial_link_text('Delivery')
        super(GeneralFunctions, self).select_item(receive_delivery)
        print('Creating new delivery...')

    def create_delivery(self):
        super(GeneralFunctions, self).scroll_to('bottom')
        time.sleep(0.5)
        self.driver.find_element_by_id(CommonLocators.SUBMIT_EDIT.value).click()
        self.confirm_stock_adjustment_warning()
        self.check_error_500()
        if self.check_error_500() is True:
            self.handle_error_500()
        else:
            pass

    def new_invoice_from_delivery(self):
        super(GeneralFunctions, self).scroll_to('bottom')
        time.sleep(0.5)
        self.driver.find_element_by_link_text('Create Invoice').click()
        print('Creating new invoice from delivery...')
        self.check_error_500()
        if self.check_error_500() is True:
            self.handle_error_500()
        else:
            pass

    def new_return(self):
        super(GeneralFunctions, self).scroll_to('bottom')
        dropup_button = self.driver.find_element_by_css_selector(CommonLocators.DROPUP_ACTIONS.value)
        super(GeneralFunctions, self).select_item(dropup_button)
        select_return = self.driver.find_element_by_partial_link_text('Return')
        super(GeneralFunctions, self).select_item(select_return)
        print('Creating new return...')

    def create_return(self):
        self.create_transaction(is_return='yes')
