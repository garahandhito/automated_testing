from enum import Enum


class TransactionDict(dict):

    def __getattr__(self, item):
        if item in self:
            return self[item]
        raise AttributeError(item)

    CREATE_TRANSACTION_TYPE = {'Sales Invoice': 'invoices/',
                               'Sales Order': 'sales_orders/',
                               'Sales Quote': 'sales_quotes/',
                               'Purchase Invoice': 'purchases/',
                               'Purchase Order': 'purchase_orders/',
                               'Purchase Quote': 'purchase_quotes/',
                               'Expense': 'expenses/',
                               'Bank Transfer': 'bank_transfers/',
                               'Bank Deposit': 'bank_deposits/',
                               'Bank Withdrawal': 'bank_withdrawals/',
                               'Asset Management': 'asset_managements/',
                               'General Journal': 'journal_entries/'}


class TransactionLocators(Enum):

    TRANSACTION_TAB = ['tab-header-1', 'tab-header-2', 'tab-header-3', 'tab-header-4']

    CHECK_JOURNAL_ENTRY = ['a.small_journal_link.get-ajax-account-transaction',
                           'a.smaller_journal_link.get-ajax-account-transaction']
    JOURNAL_ENTRY_TABLE = 'table.table.table-default.table-hover'
    JOURNAL_ENTRY = ['td.gl_total_debit', 'td.gl_total_credit', 'td.grand-total:nth-child(3)', 'td.grand-total:nth-child(4)']
    UN_REALIZED = ['td.grand-total.text-right:nth-child(2)',
                   'td.grand-total.text-right:nth-child(3):last-child',
                   'td.grand-total.text-right:nth-child(2):not(.gl_total_debit)',
                   'td.grand-total.text-right:nth-child(3):not(.gl_total_credit)']

    PAYFROM = 's2id_transaction_refund_from_id'
    PAY_LATER = 'transaction_expense_payable'
    PAYMENT_METHOD = 's2id_transaction_payment_method_id'
    TRANSACTION_ACCOUNT = ['s2id_transaction_transaction_account_lines_attributes_0_account_id',
                           's2id_transaction_transaction_account_lines_attributes_1_account_id']
    ACCOUNT_AMOUNT_DEBIT = ['transaction_transaction_account_lines_attributes_0_debit',
                            'transaction_transaction_account_lines_attributes_1_debit']
    ACCOUNT_AMOUNT_CREDIT = ['transaction_transaction_account_lines_attributes_0_credit',
                             'transaction_transaction_account_lines_attributes_1_credit']
