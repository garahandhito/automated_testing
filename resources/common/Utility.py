from enum import Enum
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException, TimeoutException
import time


class UtilityLocators(Enum):

    LOADING = 'input.select2-input.select2-focused.select2-active'
    SEARCHED = ['div.select2-result-label:nth-child(1)', 'span.select2-match']
    SELECT2_INPUT = 'input.select2-input.select2-focused'
    MODAL_BACKDROP = 'div.modal-backdrop.fade.in'
    TOP = 'div.page-title-heading:nth-child(1)'
    MIDDLE = 'div.subtotal'
    BOTTOM = 'div.copyright.col-lg-8.col-md-8.col-sm-8'


class Utility(object):

    def __init__(self, driver):
        self.driver = driver

    def wait(self, timeout=10):
        return WebDriverWait(self.driver, timeout)

    def select_item(self, item):
        action = webdriver.ActionChains(self.driver)
        action.move_to_element(item)
        action.click(item)
        action.perform()

    def input_item(self, field, item):
        self.select_item(field)
        time.sleep(0.1)
        field.send_keys(Keys.BACKSPACE)
        field.clear()
        field.send_keys(item)
        time.sleep(0.1)

    def select2_wait(self):
        return WebDriverWait(self.driver, 10).until(ec.invisibility_of_element_located(
            (By.CSS_SELECTOR, UtilityLocators.LOADING.value)))

    def select2_select_item(self):
        self.select2_wait()
        item = self.driver.find_element_by_css_selector(UtilityLocators.SEARCHED.value[0])
        self.select_item(item)

    def select2_search_and_select(self, item_select2_id, item, use_wait='yes'):
        try:
            item_select2 = self.driver.find_element_by_id(item_select2_id)
            self.select_item(item_select2)
            if use_wait == 'yes':
                self.wait().until(ec.element_to_be_clickable((By.CSS_SELECTOR, UtilityLocators.SEARCHED.value[0])))
            else:
                pass
            select2_field = self.driver.find_element_by_css_selector(UtilityLocators.SELECT2_INPUT.value)
            self.input_item(select2_field, item)
            if use_wait == 'yes':
                self.select2_wait()
            else:
                pass
            self.select2_select_item()
        except (NoSuchElementException, TimeoutException):
            print('Failed to interact with select2')
            pass

    def scroll_to(self, position):  # top, middle, and bottom
        if position == 'top':
            top = self.driver.find_element_by_css_selector(UtilityLocators.TOP.value)
            self.select_item(top)
        elif position == 'middle':
            middle = self.driver.find_element_by_css_selector(UtilityLocators.MIDDLE.value)
            self.select_item(middle)
        elif position == 'bottom':
            bottom = self.driver.find_element_by_css_selector(UtilityLocators.BOTTOM.value)
            self.select_item(bottom)
        else:
            print('unknown location to scroll')
