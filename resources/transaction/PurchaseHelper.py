from common.GeneralFunctions import GeneralFunctions


class PurchaseHelper(GeneralFunctions):

    def __init__(self, driver):
        GeneralFunctions.__init__(self, driver)
        self.driver = driver

    def set_purchase_return_date(self, date):
        super(PurchaseHelper, self).set_transaction_date(date, is_return='yes', transaction_type='purchase')

    def set_purchase_return_qty(self, line, qty):
        super(PurchaseHelper, self).set_return_product_qty('purchase', line, qty)
