from enum import Enum
from selenium.common.exceptions import NoSuchElementException
from common.GeneralFunctions import GeneralFunctions


class AssetsManagementLocators(Enum):

    ASSET_NAME = 'asset_name'
    ASSET_NUMBER = 'asset_asset_number'
    FIXED_ASSET_ACCOUNT = 's2id_asset_asset_account_id'
    ACQUISITION_DATE = 'asset_acquisition_date'
    ACQUISITION_COST = 'asset_acquisition_cost'
    ACCOUNT_CREDITED = 's2id_asset_credited_account_id'
    DEPRECIABLE = 'asset_non_depreceable'  # bukan typo, emang gini
    DEPRECIATION_METHOD = 's2id_asset_depreciation_method'
    USEFUL_LIFE = 'asset_useful_life'
    DEPRECIATION_RATE = 'asset_rate'
    DEPRECIATION_ACCOUNT = 's2id_asset_depreciation_account_id'
    ACCUMULATED_DEPRECIATION_ACC = 's2id_asset_depreciation_and_amortization_account_id'
    ACCUMULATED_DEPRECIATION = 'asset_initial_depreciation'
    INITIAL_DEPRECIATION_DATE = 'asset_initial_depreciation_asset_date'
    SELECTED_DATE = 'td.active.day'
    CREATE_ASSET = '//button[contains(text(), "Create Asset")]'
    ASSET_CREATED = '//td[contains(text(), "Created")]'


class AssetsManagementHelper(GeneralFunctions):

    def __init__(self, driver):
        GeneralFunctions.__init__(self, driver)
        self.driver = driver

    def set_asset_name(self, asset_name):
        asset_name_field = self.driver.find_element_by_id(AssetsManagementLocators.ASSET_NAME.value)
        super(AssetsManagementHelper, self).input_item(asset_name_field, asset_name)

    def set_asset_number(self, asset_number):
        asset_number_field = self.driver.find_element_by_id(AssetsManagementLocators.ASSET_NUMBER.value)
        super(AssetsManagementHelper, self).input_item(asset_number_field, asset_number)

    def set_fixed_asset_account(self, account):
        super(AssetsManagementHelper, self).select2_search_and_select(
            AssetsManagementLocators.FIXED_ASSET_ACCOUNT.value, account)

    def set_asset_related_date(self, field_id, date):
        asset_related_date_field = self.driver.find_element_by_id(field_id)
        super(AssetsManagementHelper, self).input_item(asset_related_date_field, date)
        selected_date = self.driver.find_element_by_css_selector(AssetsManagementLocators.SELECTED_DATE.value)
        super(AssetsManagementHelper, self).select_item(selected_date)

    def set_acquisition_date(self, date):
        self.set_asset_related_date(AssetsManagementLocators.ACQUISITION_DATE.value, date)

    def set_acquisition_cost(self, amount):
        acquisition_cost_field = self.driver.find_element_by_id(AssetsManagementLocators.ACQUISITION_COST.value)
        super(AssetsManagementHelper, self).input_item(acquisition_cost_field, amount)

    def set_account_credited(self, account):
        super(AssetsManagementHelper, self).select2_search_and_select(
            AssetsManagementLocators.ACCOUNT_CREDITED.value, account)

    def set_as_depreciable(self):
        super(AssetsManagementHelper, self).scroll_to('bottom')
        depreciable_tickbox = self.driver.find_element_by_id(AssetsManagementLocators.DEPRECIABLE.value)
        super(AssetsManagementHelper, self).select_item(depreciable_tickbox)

    def depreciation_method(self, method):
        super(AssetsManagementHelper, self).select2_search_and_select(
            AssetsManagementLocators.DEPRECIATION_METHOD.value, method, use_wait='no')

    def set_useful_life(self, years):
        useful_life_field = self.driver.find_element_by_id(AssetsManagementLocators.USEFUL_LIFE.value)
        super(AssetsManagementHelper, self).input_item(useful_life_field, years)

    def set_depreciation_rate(self, rate):
        depreciation_rate_field = self.driver.find_element_by_id(AssetsManagementLocators.DEPRECIATION_RATE.value)
        super(AssetsManagementHelper, self).input_item(depreciation_rate_field, rate)

    def set_depreciation_account(self, account):
        super(AssetsManagementHelper, self).select2_search_and_select(
            AssetsManagementLocators.DEPRECIATION_ACCOUNT.value, account)

    def set_accumulated_depreciation_account(self, account):
        super(AssetsManagementHelper, self).select2_search_and_select(
            AssetsManagementLocators.ACCUMULATED_DEPRECIATION_ACC.value, account)

    def set_accumulated_depreciation(self, amount):
        accumulated_depreciation_field = self.driver.find_element_by_id(
            AssetsManagementLocators.ACCUMULATED_DEPRECIATION.value)
        super(AssetsManagementHelper, self).input_item(accumulated_depreciation_field, amount)

    def set_accumulated_date(self, date):
        self.set_asset_related_date(AssetsManagementLocators.INITIAL_DEPRECIATION_DATE.value, date)

    def create_asset(self):
        create_asset_button = self.driver.find_element_by_xpath(AssetsManagementLocators.CREATE_ASSET.value)
        super(AssetsManagementHelper, self).select_item(create_asset_button)

    def check_asset_creation(self):
        try:
            self.driver.find_element_by_xpath(AssetsManagementLocators.ASSET_CREATED.value)
            print('Asset created successfully')
        except NoSuchElementException:
            print('Failed to record asset')
            raise ValueError
