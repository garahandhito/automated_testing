from common.GeneralFunctions import GeneralFunctions


class SalesHelper(GeneralFunctions):

    def __init__(self, driver):
        GeneralFunctions.__init__(self, driver)
        self.driver = driver

    def set_sales_return_date(self, date):
        super(SalesHelper, self).set_transaction_date(date, is_return='yes', transaction_type='sales')

    def set_sales_return_qty(self, line, qty):
        super(SalesHelper, self).set_return_product_qty('sales', line, qty)
