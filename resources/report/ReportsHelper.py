from enum import Enum
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException, TimeoutException
from common.GeneralFunctions import GeneralFunctions
from common.Utility import UtilityLocators
import time


class ReportsDict(dict):

    def __getattr__(self, item):
        if item in self:
            return self[item]
        raise AttributeError(item)

    REPORT_TABS = {"Business Overview": "index#tab-1",
                   "Sales": "index#tab-2",
                   "Purchases": "index#tab-3",
                   "Products": "index#tab-4",
                   "Assets": "index#tab-5",
                   "Foreign Exchange": "index#tab-6",
                   "Bank": "index#tab-7",
                   "Tax": "index#tab-8"}

    REPORTS = {"Balance Sheet": "balance_sheet",
               "General Ledger": "account_transaction",
               "Profit & Loss": "profit_loss",
               "Journal": "journal",
               "Profit & Loss New": "profit_loss_new",
               "Trial Balance": "trial_balance",
               "Cash Flow": "cash_flow",
               "Executive Summary": "executive_summary",
               "Statement of Change in Equity": "statement_of_change_in_equity",
               "Budget Management": "budget_management",
               "Profit Loss Budgeting": "profit_loss_budgeting",
               "Sales List": "sales_list",
               "Sales by Customer": "sales_by_customer",
               "Customer Balance": "customer_balance",
               "Aged Receivable": "aged_receivable",
               "Sales Delivery Report": "sales_delivery",
               "Sales by Product": "sales_by_product",
               "Sales Order Completion": "sales_order_completion",
               "Purchases List": "purchases_list",
               "Purchases by Vendor": "purchases_by_vendor",
               "Vendor Balance": "vendor_balance",
               "Expense List": "expense_list",
               "Expense Details": "expense_details",
               "Aged Payable": "aged_payable",
               "Purchases Delivery Report": "purchases_delivery",
               "Purchases by Product": "purchases_by_product",
               "Purchases Order Completion": "purchases_order_completion",
               "Inventory Summary": "inventory_summary",
               "Warehouse Stock Quantity": "warehouse_stock_quantity",
               "Inventory Valuation": "inventory_valuation",
               "Warehouse Items Valuation": "warehouse_items_valuation",
               "Inventory Details": "inventory_details",
               "Warehouse Items Stock Movement": "warehouse_items_stock_movement_summary",
               "Fixed Asset Summary": "fixed_asset_summary",
               "Fixed Asset Details": "fixed_asset_details",
               "Bank Summary": "bank_summary",
               "Realised Currency Gain/Loss": "realised_currency_gain_loss",
               "Foreign Exchange Gain & Loss": "foreign_exchange_gain_loss",
               "Unrealised Currency Gain/Loss": "unrealised_currency_gain_loss",
               "Bank Reconciliation Summary": "bank_reconciliation_summary",
               "Bank Statement Reports": "bank_statement_report",
               "Witholding Tax Reports": "witholding_tax_report",
               "Sales Tax Reports": "sales_tax_report"}

    PERIOD_FILTER_SELECT2 = {"today": 'li[value="day"]',
                                "this week": 'li[value="week"]',
                                "this month": 'li[value="month"]',
                                "this year": 'li[value="year"]',
                                "yesterday": 'li[value="yesterday"]',
                                "last week": 'li[value="last_week"]',
                                "last month": 'li[value="last_month"]',
                                "last year": 'li[value="last_year"]',
                                "this financial year": 'li[value="this_financial_report"]'}

    PERIOD_FILTER_REACTJS = {"custom": 'input[aria-activedescendant="react-select-2--option-0"]',
                                "today": 'input[aria-activedescendant="react-select-2--option-1"]',
                                "this week": 'input[aria-activedescendant="react-select-2--option-2"]',
                                "this month": 'input[aria-activedescendant="react-select-2--option-3"]',
                                "this year": 'input[aria-activedescendant="react-select-2--option-4"]',
                                "yesterday": 'input[aria-activedescendant="react-select-2--option-5"]',
                                "last week": 'input[aria-activedescendant="react-select-2--option-6"]',
                                "last month": 'input[aria-activedescendant="react-select-2--option-7"]',
                                "last year": 'input[aria-activedescendant="react-select-2--option-8"]'}

    COMPARE_PERIOD = {"today": 'li[value="day"]',
                      "weekly": 'li[value="weekly"]',
                      "monthly": 'li[value="monthly"]',
                      "quarterly": 'li[value="quarterly"]',
                      "semi yearly": 'li[value="semi_yearly"]',
                      "yearly": 'li[value="yearly"]'}

    EXPORT_FORMAT = {"PDF": 'li:nth-child(1)',
                     "XLS": 'li:nth-child(2)',
                     "CSV": 'li:nth-child(3)'}


class ReportsLocator(Enum):

    REPORT_LOADING = 'div.loading-container'

    START_DATE = ['start_date_field',
                  'input.react-bootstrap-datepicker-input-1.string.required',
                  'start_date_field_popup']
    END_DATE = ['end_date_field', 'react-bootstrap-datepicker-input-1.string.required', 'end_date_field_popup']
    SELECTED_DATE = 'td.active.day'
    PERIOD_SELECT = ['date_select', 'date_select_popup']
    PERIODS = '//*[@id="date_select"]/ul'

    FILTER_BUTTON = ['report_filter_button',
                     'button.btn.btn-action-flat.balance-sheet-filter-btn-2.new-btn',
                     'report_filter_button_modal']
    MORE_FILTER_BUTTON = 'more-filter'
    AS_OF = 'end_date_field_popup'
    COMPARE_PERIOD = 'compare-period'
    MODAL_FILTER_BUTTON = 'report_filter_button_modal'
    TAGS_FIELD = 's2id_tag_select'
    SELECT2_INPUT = 'input.select2-input.select2-focused'
    FILTER_BY_ACCOUNT = 's2id_account_select'
    ACCOUNTS_DROPDOWN = 'select2-drop'

    REPORT_BODY = 'table.report-table.table'
    ERROR_500 = 'modj_errors'
    HANDLE_ERROR_500 = '//figure[@class="logo-small"]'

    EXPORT_BUTTON = 'button.btn.btn-action-flat.dropdown-toggle.new-btn.btn-export'
    EXPORT_DROPDOWN = 'ul.dropdown-menu.dropdown-menu-right.mid-z-index'
    PDF_REPORT = 'embed[type="application/pdf"]'


class ReportsHelper(GeneralFunctions):

    def __init__(self, driver):
        GeneralFunctions.__init__(self, driver)

    def open_report(self, report_name):
        self.driver.find_element_by_id('vnav-dashboard-link').click()
        environment = self.driver.current_url
        report_url = environment + 'reports/' + ReportsDict.REPORTS[report_name]
        self.driver.get(report_url)
        print('Opening ' + report_name + ' report...')

    def check_report_loading_success(self):
        try:
            WebDriverWait(self.driver, 10).until(
                ec.presence_of_all_elements_located((By.CSS_SELECTOR, ReportsLocator.REPORT_BODY.value)))
            return True
        except (TimeoutException, NoSuchElementException):
            return False

    def check_loading_report(self):
        if self.check_report_loading_success() is True:
            print('Report loaded successfully')
            print(' ')
        else:
            raise ValueError

    def assess_report_loading(self):
        self.check_report_loading_success()
        self.check_error_500()
        if (self.check_report_loading_success() is False) and (self.check_error_500() is True):
            self.handle_error_500()
        elif (self.check_report_loading_success() is True) and (self.check_error_500() is False):
            print('Report loaded successfully')
            print('PASSED\n')
        elif (self.check_report_loading_success() is False) and (self.check_error_500() is False):
            print('Failed to load report')
            print('FAILED\n')
        else:
            self.handle_error_500()

    def set_date_filter(self, date, date_type='start', at_popup='no'):
        try:
            WebDriverWait(self.driver, 20).until(
                ec.invisibility_of_element_located((By.CSS_SELECTOR, ReportsLocator.REPORT_LOADING.value)))
            if date_type != 'start':
                if at_popup == 'no':
                    try:
                        date_field = self.driver.find_element_by_id(ReportsLocator.END_DATE.value[0])
                    except NoSuchElementException:
                        date_field = self.driver.find_element_by_css_selector(ReportsLocator.END_DATE.value[1])
                else:
                    date_field = self.driver.find_element_by_id(ReportsLocator.END_DATE.value[2])
            else:
                if at_popup == 'no':
                    try:
                        date_field = self.driver.find_element_by_id(ReportsLocator.START_DATE.value[0])
                    except NoSuchElementException:
                        date_field = self.driver.find_element_by_css_selector(ReportsLocator.START_DATE.value[1])
                else:
                    date_field = self.driver.find_element_by_id(ReportsLocator.START_DATE.value[2])
            super(ReportsHelper, self).input_item(date_field, date)
            selected_date = self.driver.find_element_by_css_selector(ReportsLocator.SELECTED_DATE.value)
            super(ReportsHelper, self).select_item(selected_date)
        except (NoSuchElementException, TimeoutException):
            pass

    def custom_date_filter(self, start_date, end_date):
        self.set_date_filter(start_date, date_type='start')
        self.set_date_filter(end_date, date_type='end')
        self.apply_filter()
        print('Filter by custom date')

    def filter_by_period(self, period, at_popup='no'):
        if at_popup == 'no':
            period_select = self.driver.find_element_by_id(ReportsLocator.PERIOD_SELECT.value[0])
        else:
            period_select = self.driver.find_element_by_id(ReportsLocator.PERIOD_SELECT.value[1])
        super(ReportsHelper, self).select_item(period_select)
        WebDriverWait(self.driver, 5).until(
            ec.presence_of_all_elements_located((By.XPATH, ReportsLocator.PERIODS.value)))
        if at_popup == 'no':
            selected_period = self.driver.find_element_by_css_selector(ReportsDict.PERIOD_FILTER_SELECT2[period])
        else:
            selected_period = period_select.find_element_by_css_selector(ReportsDict.PERIOD_FILTER_SELECT2[period])
        super(ReportsHelper, self).select_item(selected_period)
        print('Filter by period: ' + period)

    def try_all_periods(self):
        periods_list = list(ReportsDict.PERIOD_FILTER_SELECT2.values())
        for item in periods_list:
            try:
                period_select = self.driver.find_element_by_id(ReportsLocator.PERIOD_SELECT.value)
                super(ReportsHelper, self).select_item(period_select)
                WebDriverWait(self.driver, 5).until(
                    ec.presence_of_all_elements_located((By.XPATH, ReportsLocator.PERIODS.value)))
                time.sleep(0.5)
                period = self.driver.find_element_by_css_selector(item)
                super(ReportsHelper, self).select_item(period)
                for period_name, locator in ReportsDict.PERIOD_FILTER_SELECT2.items():
                    if locator == item:
                        print('Filter by period: ' + period_name)
                self.assess_report_loading()
            except NoSuchElementException:
                pass

    def apply_filter(self, at_popup='no'):
        if at_popup == 'no':
            try:
                filter_button = self.driver.find_element_by_id(ReportsLocator.FILTER_BUTTON.value[0])
            except NoSuchElementException:
                filter_button = self.driver.find_element_by_css_selector(ReportsLocator.FILTER_BUTTON.value[1])
        else:
            filter_button = self.driver.find_element_by_id(ReportsLocator.FILTER_BUTTON.value[2])
        super(ReportsHelper, self).select_item(filter_button)
        print('Applying filter...')

    def open_more_filter(self):
        try:
            more_filter_button = self.driver.find_element_by_id(ReportsLocator.MORE_FILTER_BUTTON.value)
            super(ReportsHelper, self).select_item(more_filter_button)
            WebDriverWait(self.driver, 5).until(
                ec.element_to_be_clickable((By.ID, ReportsLocator.FILTER_BUTTON.value[2])))
        except NoSuchElementException:
            pass

    def group_by_tag(self, tags):
        try:
            for tag in tags:
                tags_field = self.driver.find_element_by_id(ReportsLocator.TAGS_FIELD.value)
                self.select_item(tags_field)
                input_tags = self.driver.find_element_by_css_selector(ReportsLocator.SELECT2_INPUT.value)
                super(ReportsHelper, self).input_item(input_tags, tag)
                super(ReportsHelper, self).select2_wait()
                input_tags.send_keys(Keys.RETURN)
        except NoSuchElementException:
            pass

    def compare_period(self, previous_period):  # previous_period is int from 1 - 11
        WebDriverWait(self.driver, 5).until(ec.element_to_be_clickable((By.ID, ReportsLocator.COMPARE_PERIOD.value)))
        compare_period = self.driver.find_element_by_id(ReportsLocator.COMPARE_PERIOD.value)
        super(ReportsHelper, self).select_item(compare_period)
        try:
            WebDriverWait(self.driver, 1).until(
                ec.element_to_be_clickable((By.CSS_SELECTOR, 'li[compare-period-value="' + previous_period + '"]')))
            selected_previous_period = compare_period.find_element_by_css_selector(
                'li[compare-period-value="' + previous_period + '"]')
            super(ReportsHelper, self).select_item(selected_previous_period)
            print('Comparing with ' + previous_period + ' previous periods')
        except (NoSuchElementException, TimeoutException):
            print('This report does not support comparing periods with the given number')

    def filter_by_account(self, accounts):
        account_filter = self.driver.find_element_by_id(ReportsLocator.FILTER_BY_ACCOUNT.value)
        super(ReportsHelper, self).select_item(account_filter)
        WebDriverWait(self.driver, 2).until(
            ec.presence_of_all_elements_located((By.ID, ReportsLocator.ACCOUNTS_DROPDOWN.value)))
        for account in accounts:
            input_account_field = account_filter.find_element_by_css_selector(UtilityLocators.SELECT2_INPUT.value)
            super(ReportsHelper, self).input_item(input_account_field, account)
            WebDriverWait(self.driver, 1).until(ec.element_to_be_clickable(
                (By.CSS_SELECTOR, UtilityLocators.SEARCHED.value[0])))
            selected_account = self.driver.find_element_by_css_selector(UtilityLocators.SEARCHED.value[0])
            super(ReportsHelper, self).select_item(selected_account)
            time.sleep(0.5)

    def export_report(self, report_format):
        export_button = self.driver.find_element_by_css_selector(ReportsLocator.EXPORT_BUTTON.value)
        super(ReportsHelper, self).select_item(export_button)
        WebDriverWait(self.driver, 1).until(
            ec.visibility_of_element_located((By.CSS_SELECTOR, ReportsLocator.EXPORT_DROPDOWN.value)))
        time.sleep(0.5)
        export_dropdown = self.driver.find_element_by_css_selector(ReportsLocator.EXPORT_DROPDOWN.value)
        try:
            export_format = export_dropdown.find_element_by_css_selector(ReportsDict.EXPORT_FORMAT[report_format])
            super(ReportsHelper, self).select_item(export_format)
            print('Exporting report to ' + report_format)
            if report_format == 'PDF':
                assert (self.driver.find_element_by_css_selector(ReportsLocator.PDF_REPORT.value)), "Failed"
                print("Success")
                self.driver.back()
            else:
                time.sleep(3)
        except NoSuchElementException:
            print('Cannot export report, too many columns')
            print(' ')

    def check_all_reports(self, start_date, end_date):
        for report in ReportsDict.REPORTS:
            self.open_report(report)
            self.check_error_500()
            if self.check_error_500() is False:
                self.custom_date_filter(start_date, end_date)
                self.try_all_periods()
            else:
                self.handle_error_500()
        self.driver.quit()
