from selenium import webdriver
from jurnal_navigations import Navigations
from salesquote_helper import SalesQuoteHelper
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import ElementNotSelectableException
from selenium.common.exceptions import WebDriverException

chromeOptions = Options()
chromeOptions.add_argument("--kiosk")
driver = webdriver.Chrome(chrome_options=chromeOptions)
n = Navigations(driver)
sq = SalesQuoteHelper(driver)

n.open_newblueranger()
n.login_aim()
# n.activate_automation_company()
n.open_sales_index()

sq.create_new_salesquote()
sq.select_customer('Costumer 1')
sq.set_transaction_date('01/01/2018')
sq.select_tags('tag1')
sq.tax_inclusive_toggle()
sq.scroll_page()
sq.select_product('produk 1')
sq.add_qty('10')
sq.add_price('10000')
sq.add_discount('10')
sq.select_tax('PPN')
sq.click_anywhere()
sq.add_message('automated testing')
sq.add_memo('automated')
sq.click_create_button()
sq.check_if_created()

n.close_browser()