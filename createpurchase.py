from selenium import webdriver
from jurnal_navigations import Navigations
from purchase_helper import PurchaseInvoiceHelper
from selenium.webdriver.chrome.options import Options

chromeOptions = Options()
chromeOptions.add_argument("--kiosk")
driver = webdriver.Chrome()
#driver = webdriver.Chrome(chrome_options=chromeOptions)
n = Navigations(driver)
pi = PurchaseInvoiceHelper(driver)

n.open_newblueranger()
n.login_irene()
# n.activate_automation_company()
n.open_purchases_index()

pi.create_new_purchaseinvoice()
pi.select_vendor('irene')
pi.tick_shipping()
pi.transaction_address('jl. auto auto no. 110')
pi.ship_via('jne')
pi.tracking_number('123456789')
pi.set_transaction_date('01/01/2018')
pi.select_tags('tag1')
pi.tax_inclusive_toggle()
pi.scroll_page()
pi.select_product('produk 1')
pi.add_qty('10')
pi.add_price('10000')
pi.add_discount('10')
pi.select_tax('PPN')
pi.click_anywhere()
pi.add_message('automated testing')
pi.add_memo('automated')
pi.click_create_button()
pi.check_if_created()

n.view_journal_entry()
n.check_journal_entry()
n.close_browser()