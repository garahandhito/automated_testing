import unittest
from cases.transaction.test_cashbank import TestCashBank
from cases.reports.test_general_ledger import TestGeneralLedger

test_cashbank = unittest.TestLoader().loadTestsFromTestCase(TestCashBank)
test_general_ledger = unittest.TestLoader().loadTestsFromTestCase(TestGeneralLedger)

test_suite = unittest.TestSuite([test_cashbank, test_general_ledger])

unittest.TextTestRunner(verbosity=2).run(test_suite)
