import HtmlTestRunner
import unittest
from selenium import webdriver
from report import BalanceSheetController
from resources.cred.gara import Gara


class TestBalanceSheet(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Chrome()
        cls.driver.get('http://jurnal.ap-southeast-1.elasticbeanstalk.com')
        cls.driver.maximize_window()
        bs = BalanceSheetController.BalanceSheetController(cls.driver)
        bs.login_as(Gara.EMAIL.value, Gara.PASSWORD.value)

    def test_bs(self):
        """ Open Balance Sheet """
        bs = BalanceSheetController.BalanceSheetController(self.driver)
        bs.open_report('Balance Sheet')
        bs.check_loading_report()

    def test_bs_custom(self):
        """ Balance Sheet with custom date filter """
        bs = BalanceSheetController.BalanceSheetController(self.driver)
        bs.custom_date_filter('01/01/2017', '01/01/2018')
        bs.apply_filter()
        bs.check_loading_report()

    def test_bs_today(self):
        """ Balance Sheet with today period filter """
        bs = BalanceSheetController.BalanceSheetController(self.driver)
        bs.filter_by_period('today')
        bs.apply_filter()
        bs.check_loading_report()

    def test_bs_this_week(self):
        """ Balance Sheet with this week period filter """
        bs = BalanceSheetController.BalanceSheetController(self.driver)
        bs.filter_by_period('this week')
        bs.apply_filter()
        bs.check_loading_report()

    def test_bs_this_month(self):
        """ Balance Sheet with this month period filter """
        bs = BalanceSheetController.BalanceSheetController(self.driver)
        bs.filter_by_period('this month')
        bs.apply_filter()
        bs.check_loading_report()

    def test_bs_this_year(self):
        """ Balance Sheet with this year period """
        bs = BalanceSheetController.BalanceSheetController(self.driver)
        bs.filter_by_period('this year')
        bs.apply_filter()
        bs.check_loading_report()

    def test_bs_yesterday(self):
        """ Balance Sheet with yesterday period filter """
        bs = BalanceSheetController.BalanceSheetController(self.driver)
        bs.filter_by_period('yesterday')
        bs.apply_filter()
        bs.check_loading_report()

    def test_bs_last_week(self):
        """ Balance Sheet with last week period filter """
        bs = BalanceSheetController.BalanceSheetController(self.driver)
        bs.filter_by_period('last week')
        bs.apply_filter()
        bs.check_loading_report()

    def test_bs_last_month(self):
        """ Balance Sheet with last month period filter """
        bs = BalanceSheetController.BalanceSheetController(self.driver)
        bs.filter_by_period('last month')
        bs.apply_filter()
        bs.check_loading_report()

    def test_bs_last_year(self):
        """ Balance Sheet with last year period filter """
        bs = BalanceSheetController.BalanceSheetController(self.driver)
        bs.filter_by_period('last year')
        bs.apply_filter()
        bs.check_loading_report()

    def test_BalanceSheet_1(self):
        """ filter: this month, compare to: all previous periods, order: desc, export to: PDF """
        count = 1
        while count < 12:
            bs = BalanceSheetController.BalanceSheetController(self.driver)
            bs.open_report('Balance Sheet')
            bs.open_more_filter()
            bs.filter_by_period('this month', at_popup='yes')
            bs.period_comparison('monthly')
            bs.compare_period(str(count))
            bs.apply_filter(at_popup='yes')
            bs.check_loading_report()
            bs.export_report('PDF')
            count += 1

    # def test_BalanceSheet_2(self):
    #     """ filter: this month, compare to: all previous periods, order: asc, export to: PDF """
    #     count = 1
    #     while count < 12:
    #         bs = BalanceSheetController.BalanceSheetController(self.driver)
    #         bs.open_report('Balance Sheet')
    #         bs.open_more_filter()
    #         bs.filter_by_period('this month', at_popup='yes')
    #         bs.period_comparison('monthly')
    #         bs.compare_period(str(count))
    #         bs.order_period('ascending')
    #         bs.apply_filter(at_popup='yes')
    #         bs.check_loading_report()
    #         bs.export_report('PDF')
    #         count += 1

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()


if __name__ == "__main__":
    unittest.main(testRunner=HtmlTestRunner.HTMLTestRunner(output=r'..\..\results\balance_sheet'))
