import HtmlTestRunner
import unittest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from report import GeneralLedgerController
from resources.cred.gara import Gara


class TestGeneralLedger(unittest.TestCase):

    def setUp(self):
        options = Options()
        # options.add_argument("--headless")
        options.add_argument('start-maximized')
        self.driver = webdriver.Chrome(options=options)
        gl = GeneralLedgerController.GeneralLedgerController(self.driver)
        gl.open_environment('jurnal-ledger')
        gl.login_as(Gara.EMAIL.value, Gara.PASSWORD.value)

    def test_general_ledger(self):
        """ test General Ledger """
        gl = GeneralLedgerController.GeneralLedgerController(self.driver)
        gl.open_report('General Ledger')
        gl.open_more_filter()
        gl.filter_by_account(['1-10001', '1-10002'])
        gl.apply_filter(at_popup='yes')
        gl.check_loading_report()
        gl.export_report('PDF')
        gl.export_report('XLS')
        gl.export_report('CSV')

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main(testRunner=HtmlTestRunner.HTMLTestRunner(output='general ledger'))
