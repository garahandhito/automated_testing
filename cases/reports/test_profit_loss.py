import HtmlTestRunner
import unittest
from selenium import webdriver
from report import ProfitLossController
from resources.cred.gara import Gara


class TestProfitLoss(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Chrome()
        cls.driver.get('http://jurnal.ap-southeast-1.elasticbeanstalk.com')
        cls.driver.maximize_window()
        pl = ProfitLossController.ProfitLossController(cls.driver)
        pl.login_as(Gara.EMAIL.value, Gara.PASSWORD.value)

    def test_pl_1(self):
        """ filter custom date """
        pl = ProfitLossController.ProfitLossController(self.driver)
        pl.open_report('Profit & Loss')
        pl.custom_date_filter('01/01/2017', '01/10/2017')
        pl.check_loading_report()

    def test_pl_2(self):
        """ using more filters """
        pl = ProfitLossController.ProfitLossController(self.driver)
        pl.open_more_filter()
        pl.filter_by_period('this month', at_popup='yes')
        pl.compare_period('6')
        pl.apply_filter(at_popup='yes')
        pl.check_loading_report()

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()


if __name__ == "__main__":
    unittest.main(testRunner=HtmlTestRunner.HTMLTestRunner(output='balance sheet'))
