import HtmlTestRunner
import unittest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from resources.transaction import CashBankHelper
from resources.cred.gara import Gara


class TestCashBank(unittest.TestCase):

    def setUp(self):
        options = Options()
        options.add_argument("--headless")
        options.add_argument('start-maximized')
        self.driver = webdriver.Chrome(options=options)
        cbh = CashBankHelper.CashBankHelper(self.driver)
        cbh.open_environment('jurnal-ledger')
        cbh.login_as(Gara.EMAIL.value, Gara.PASSWORD.value)

    def test_bank_transfer(self):
        """ Test Bank Transfer """
        cbh = CashBankHelper.CashBankHelper(self.driver)
        cbh.new_transaction('Bank Transfer')
        cbh.transfer_from('1-10002')
        cbh.deposit_to('1-10001')
        cbh.set_transfer_amount('1000000')
        cbh.set_transaction_date('01/03/2018')
        cbh.create_transaction()
        cbh.check_journal_entry()

    def test_bank_deposit(self):
        """ Test Bank Deposit """
        cbh = CashBankHelper.CashBankHelper(self.driver)
        cbh.new_transaction('Bank Deposit')
        cbh.deposit_to('1-10002')
        cbh.select_person('customer 1')
        cbh.set_transaction_date('02/03/2018')
        cbh.add_tags(['tag1', 'tag2'])
        cbh.add_account_line(1, '1-10300', '', '250000', 'deposit')
        cbh.add_account_line(2, '1-10301', '', '150000', 'deposit')
        cbh.create_transaction()
        cbh.check_journal_entry()

    def test_bank_withdrawal(self):
        """ Test Bank Withdrawal """
        cbh = CashBankHelper.CashBankHelper(self.driver)
        cbh.new_transaction('Bank Withdrawal')
        cbh.transfer_from('1-10002')
        cbh.select_person('customer 1')
        cbh.set_transaction_date('03/03/2018')
        cbh.add_tags(['tag2', 'tag3'])
        cbh.add_account_line(1, '1-10300', '', '250000', 'withdraw')
        cbh.add_account_line(2, '1-10301', '', '150000', 'withdraw')
        cbh.create_transaction()
        cbh.check_journal_entry()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main(testRunner=HtmlTestRunner.HTMLTestRunner(output='cashbank'))
