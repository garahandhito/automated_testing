import HtmlTestRunner
import unittest
from selenium import webdriver
from resources.transaction import AssetsManagementHelper
from resources.cred.gara import Gara


class TestRecordAsset(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Chrome()
        cls.driver.get('http://jurnal.ap-southeast-1.elasticbeanstalk.com')
        cls.driver.maximize_window()
        amh = AssetsManagementHelper.AssetsManagementHelper(cls.driver)
        amh.login_as(Gara.EMAIL.value, Gara.PASSWORD.value)

    def test_1(self):
        """ Record non-depreciable asset """
        amh = AssetsManagementHelper.AssetsManagementHelper(self.driver)
        amh.new_transaction('Asset Management')
        amh.set_asset_name('Tanah 1')
        amh.set_asset_number('TNH-001')
        amh.set_fixed_asset_account('1-10700')
        amh.set_acquisition_date('01/02/2017')
        amh.set_acquisition_cost('500000000')
        amh.set_account_credited('1-10002')
        amh.add_tags(['tanah'], is_asset='yes')
        amh.create_asset()
        amh.check_asset_creation()

    def test_2(self):
        """ Record depreciable asset """
        amh = AssetsManagementHelper.AssetsManagementHelper(self.driver)
        amh.new_transaction('Asset Management')
        amh.set_asset_name('Motor 1')
        amh.set_asset_number('MTR-001')
        amh.set_fixed_asset_account('1-10703')
        amh.set_acquisition_date('01/03/2017')
        amh.set_acquisition_cost('17000000')
        amh.set_account_credited('1-10002')
        amh.add_tags(['motor'], is_asset='yes')
        amh.set_as_depreciable()
        amh.set_useful_life('10')
        amh.set_depreciation_account('6-60502')
        amh.set_accumulated_depreciation_account('1-10753')
        amh.create_asset()
        amh.check_asset_creation()

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()


if __name__ == "__main__":
    unittest.main(testRunner=HtmlTestRunner.HTMLTestRunner(output='asset'))
