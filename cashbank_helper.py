from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException, TimeoutException, ElementNotVisibleException
import time


class CashBankHelper(object):

    # static variables
    finish_loading = ec.invisibility_of_element_located(
        (By.CSS_SELECTOR, 'input.select2-input.select2-focused.select2-active'))

    searched_item = 'div.select2-result-label:nth-child(1)'

    def __init__(self, driver):
        self.driver = driver

    def show_archived_accounts(self):
        self.driver.find_element_by_id('_show_archived').click()

    def create_new_account(self):
        self.driver.find_element_by_css_selector('a[href="/registers/new"]').click()

    def new_bank_transfer(self):
        self.driver.find_element_by_css_selector(
            'button.btn.btn-action-recon.dropdown-toggle.btn-dropdown-action').click()
        self.driver.find_element_by_css_selector('a[href="/bank_transfers/new"]').click()

    def new_bank_deposit(self):
        self.driver.find_element_by_css_selector(
            'button.btn.btn-action-recon.dropdown-toggle.btn-dropdown-action').click()
        self.driver.find_element_by_css_selector('a[href="/bank_deposits/new"]').click()

    def new_bank_withdrawal(self):
        self.driver.find_element_by_css_selector(
            'button.btn.btn-action-recon.dropdown-toggle.btn-dropdown-action').click()
        self.driver.find_element_by_css_selector('a[href="/bank_withdrawals/new"]').click()

    def is_using_mc(self):
        try:
            self.driver.find_element_by_id('s2id_transaction_currency_list_id')
            return True
        except NoSuchElementException:
            return False

    def bank_rules(self):
        self.driver.find_element_by_css_selector('a[href="/bank_rules"]').click()

# buat sekarang, pilih link account baru nemu cara pake account id
    def cash_account(self):
        self.driver.find_element_by_css_selector('a[href="/registers/detail/299403"]').click()

    def bank_account(self):
        self.driver.find_element_by_css_selector('a[href="/registers/detail/299404"]').click()

    def bs_cash(self):
        self.driver.find_element_by_css_selector('div.width100percent:nth-child(1)').click()
        print('Import page for Cash is opened')

    def bs_bank(self):
        self.driver.find_element_by_css_selector('a.href="/import/bank_statements?createdFrom=299404"').click()
        print('Import page for Bank is opened')

# fill the value "path" with the BS file path in your computer
    def bs_import(self, path):
        self.driver.find_element_by_id('import_file').send_keys(path)
        self.driver.find_element_by_name('commit').click()
        self.driver.find_element_by_name('button').click()
        self.driver.find_element_by_css_selector('a.btn-success').click()
        print('Bank statement has been uploaded successfully')

    def transfer_from(self, account_number):
        self.driver.find_element_by_id('s2id_transaction_refund_from_id').click()
        WebDriverWait(self.driver, 10).until(self.finish_loading)
        self.driver.find_element_by_id('s2id_autogen7_search').send_keys(account_number)
        try:
            WebDriverWait(self.driver, 10).until(self.finish_loading)
        finally:
            self.driver.find_element_by_css_selector(self.searched_item).click()

    def transfer_to(self, account_number):
        self.driver.find_element_by_id('s2id_transaction_deposit_to_id').click()
        WebDriverWait(self.driver, 10).until(self.finish_loading)
        self.driver.find_element_by_id('s2id_autogen8_search').send_keys(account_number)
        try:
            WebDriverWait(self.driver, 10).until(self.finish_loading)
        finally:
            self.driver.find_element_by_css_selector(self.searched_item).click()

    def deposit_to(self, account_number):
        self.is_using_mc()
        self.driver.find_element_by_id('s2id_transaction_deposit_to_id').click()
        WebDriverWait(self.driver, 10).until(self.finish_loading)
        if self.is_using_mc() is True:
            select2_deposit_to = 's2id_autogen15_search'
        else:
            select2_deposit_to = 's2id_autogen14_search'
        self.driver.find_element_by_id(select2_deposit_to).send_keys(account_number)
        try:
            WebDriverWait(self.driver, 10).until(self.finish_loading)
        finally:
            self.driver.find_element_by_css_selector(self.searched_item).click()

    def withdraw_from(self, account_number):
        self.is_using_mc()
        self.driver.find_element_by_id('s2id_transaction_refund_from_id').click()
        WebDriverWait(self.driver, 10).until(self.finish_loading)
        if self.is_using_mc() is True:
            select2_withdraw_from = 's2id_autogen13_search'
        else:
            select2_withdraw_from = 's2id_autogen12_search'
        self.driver.find_element_by_id(select2_withdraw_from).send_keys(account_number)
        try:
            WebDriverWait(self.driver, 10).until(self.finish_loading)
        finally:
            self.driver.find_element_by_css_selector(self.searched_item).click()

    def transfer_amount(self, amount):
        transfer_amount = self.driver.find_element_by_id('transaction_transfer_amount')
        transfer_amount.click()
        time.sleep(0.5)
        transfer_amount.clear()
        transfer_amount.send_keys(amount)
        self.driver.find_element_by_css_selector('label.control-label.amount-due').click()

    def add_transfer_tags(self, tags):
        self.is_using_mc()
        for tag in tags:
            self.driver.find_element_by_id('s2id_transaction_tag_ids').click()
            # if self.is_using_mc() is True:
            #     select2_tag = 's2id_autogen13'
            # else:
            #     select2_tag = 's2id_autogen12'
            WebDriverWait(self.driver, 3).until(self.finish_loading)
            self.driver.find_element_by_id('s2id_autogen4').send_keys(tag)
            WebDriverWait(self.driver, 3).until(self.finish_loading)
            self.driver.find_element_by_id('s2id_autogen4').send_keys(Keys.RETURN)
            self.driver.find_element_by_css_selector('label.control-label').click()

    def add_deposit_tags(self, tags):
        self.is_using_mc()
        for tag in tags:
            self.driver.find_element_by_id('s2id_transaction_tag_ids').click()
            if self.is_using_mc() is True:
                select2_tag = 's2id_autogen8'
            else:
                select2_tag = 's2id_autogen7'
            WebDriverWait(self.driver, 3).until(self.finish_loading)
            self.driver.find_element_by_id(select2_tag).send_keys(tag)
            WebDriverWait(self.driver, 3).until(self.finish_loading)
            self.driver.find_element_by_id(select2_tag).send_keys(Keys.RETURN)
            self.driver.find_element_by_css_selector('label.control-label').click()

    def add_withdrawal_tags(self, tags):
        self.is_using_mc()
        for tag in tags:
            self.driver.find_element_by_id('s2id_transaction_tag_ids').click()
            if self.is_using_mc() is True:
                select2_tag = 's2id_autogen7'
            else:
                select2_tag = 's2id_autogen6'
            WebDriverWait(self.driver, 3).until(self.finish_loading)
            self.driver.find_element_by_id(select2_tag).send_keys(tag)
            WebDriverWait(self.driver, 3).until(self.finish_loading)
            self.driver.find_element_by_id(select2_tag).send_keys(Keys.RETURN)
            self.driver.find_element_by_css_selector('label.control-label').click()

    def select_payer(self, payer):
        self.is_using_mc()
        self.driver.find_element_by_id('s2id_transaction_person_id').click()
        WebDriverWait(self.driver, 10).until(self.finish_loading)
        if self.is_using_mc() is True:
            select2_payer = 's2id_autogen9_search'
        else:
            select2_payer = 's2id_autogen8_search'
        self.driver.find_element_by_id(select2_payer).send_keys(payer)
        try:
            WebDriverWait(self.driver, 10).until(self.finish_loading)
        finally:
            self.driver.find_element_by_css_selector(self.searched_item).click()

    def select_payee(self, payee):
        self.is_using_mc()
        self.driver.find_element_by_id('s2id_transaction_person_id').click()
        WebDriverWait(self.driver, 10).until(self.finish_loading)
        if self.is_using_mc() is True:
            select2_payer = 's2id_autogen9_search'
        else:
            select2_payer = 's2id_autogen8_search'
        self.driver.find_element_by_id(select2_payer).send_keys(payee)
        try:
            WebDriverWait(self.driver, 10).until(self.finish_loading)
        finally:
            self.driver.find_element_by_css_selector(self.searched_item).click()

    def set_transaction_date(self, date):
        date_field = self.driver.find_element_by_id('transaction_transaction_date')
        date_field.click()
        time.sleep(0.5)
        date_field.clear()
        date_field.send_keys(date)
        self.driver.find_element_by_css_selector('td.active.day').click()

    def first_receive_from(self, account_number, tax, amount):
        self.is_using_mc()

        # select first account
        self.driver.find_element_by_xpath('//span[contains(text(), "Select account")]').click()
        WebDriverWait(self.driver, 10).until(self.finish_loading)
        if self.is_using_mc() is True:
            first_receive_from_account = 's2id_autogen16_search'
        else:
            first_receive_from_account = 's2id_autogen15_search'
        self.driver.find_element_by_id(first_receive_from_account).send_keys(account_number)
        try:
            WebDriverWait(self.driver, 10).until(self.finish_loading)
        finally:
            self.driver.find_element_by_css_selector(self.searched_item).click()

        # select first tax
        self.driver.find_element_by_id('s2id_transaction_transaction_account_lines_attributes_0_line_tax_id').click()
        WebDriverWait(self.driver, 10).until(self.finish_loading)
        if self.is_using_mc() is True:
            first_tax = 's2id_autogen10_search'
        else:
            first_tax = 's2id_autogen9_search'
        self.driver.find_element_by_id(first_tax).send_keys(tax)
        try:
            WebDriverWait(self.driver, 10).until(self.finish_loading)
        finally:
            self.driver.find_element_by_css_selector(self.searched_item).click()

        # set first amount
        first_amount = self.driver.find_element_by_id('transaction_transaction_account_lines_attributes_0_credit')
        first_amount.click()
        time.sleep(0.5)
        first_amount.clear()
        first_amount.send_keys(amount)
        self.driver.find_element_by_css_selector('thead.table-header').click()

    def second_receive_from(self, account_number, tax, amount):
        self.is_using_mc()

        # select second account
        self.driver.find_element_by_xpath('//span[contains(text(), "Select account")]').click()
        WebDriverWait(self.driver, 10).until(self.finish_loading)
        if self.is_using_mc() is True:
            first_receive_from_account = 's2id_autogen17_search'
        else:
            first_receive_from_account = 's2id_autogen16_search'
        self.driver.find_element_by_id(first_receive_from_account).send_keys(account_number)
        try:
            WebDriverWait(self.driver, 10).until(self.finish_loading)
        finally:
            self.driver.find_element_by_css_selector(self.searched_item).click()

        # select second tax
        self.driver.find_element_by_id('s2id_transaction_transaction_account_lines_attributes_1_line_tax_id').click()
        WebDriverWait(self.driver, 10).until(self.finish_loading)
        if self.is_using_mc() is True:
            first_tax = 's2id_autogen11_search'
        else:
            first_tax = 's2id_autogen10_search'
        self.driver.find_element_by_id(first_tax).send_keys(tax)
        try:
            WebDriverWait(self.driver, 10).until(self.finish_loading)
        finally:
            self.driver.find_element_by_css_selector(self.searched_item).click()

        # set second amount
        first_amount = self.driver.find_element_by_id('transaction_transaction_account_lines_attributes_1_credit')
        first_amount.click()
        time.sleep(0.5)
        first_amount.clear()
        first_amount.send_keys(amount)
        self.driver.find_element_by_css_selector('thead.table-header').click()

    def first_payment_for(self, account_number, tax, amount):
        self.is_using_mc()

        # select first account
        self.driver.find_element_by_xpath('//span[contains(text(), "Select account")]').click()
        WebDriverWait(self.driver, 10).until(self.finish_loading)
        if self.is_using_mc() is True:
            first_receive_from_account = 's2id_autogen14_search'
        else:
            first_receive_from_account = 's2id_autogen13_search'
        self.driver.find_element_by_id(first_receive_from_account).send_keys(account_number)
        try:
            WebDriverWait(self.driver, 10).until(self.finish_loading)
        finally:
            self.driver.find_element_by_css_selector(self.searched_item).click()

        # select first tax
        self.driver.find_element_by_id('s2id_transaction_transaction_account_lines_attributes_0_line_tax_id').click()
        WebDriverWait(self.driver, 10).until(self.finish_loading)
        if self.is_using_mc() is True:
            first_tax = 's2id_autogen9_search'
        else:
            first_tax = 's2id_autogen8_search'
        self.driver.find_element_by_id(first_tax).send_keys(tax)
        try:
            WebDriverWait(self.driver, 10).until(self.finish_loading)
        finally:
            self.driver.find_element_by_css_selector(self.searched_item).click()

        # set first amount
        first_amount = self.driver.find_element_by_id('transaction_transaction_account_lines_attributes_0_debit')
        first_amount.click()
        time.sleep(0.5)
        first_amount.clear()
        first_amount.send_keys(amount)
        self.driver.find_element_by_css_selector('thead.table-header').click()

    def second_payment_for(self, account_number, tax, amount):
        self.is_using_mc()

        # select first account
        self.driver.find_element_by_xpath('//span[contains(text(), "Select account")]').click()
        WebDriverWait(self.driver, 10).until(self.finish_loading)
        if self.is_using_mc() is True:
            first_receive_from_account = 's2id_autogen15_search'
        else:
            first_receive_from_account = 's2id_autogen14_search'
        self.driver.find_element_by_id(first_receive_from_account).send_keys(account_number)
        try:
            WebDriverWait(self.driver, 10).until(self.finish_loading)
        finally:
            self.driver.find_element_by_css_selector(self.searched_item).click()

        # select first tax
        self.driver.find_element_by_id('s2id_transaction_transaction_account_lines_attributes_1_line_tax_id').click()
        WebDriverWait(self.driver, 10).until(self.finish_loading)
        if self.is_using_mc() is True:
            first_tax = 's2id_autogen10_search'
        else:
            first_tax = 's2id_autogen9_search'
        self.driver.find_element_by_id(first_tax).send_keys(tax)
        try:
            WebDriverWait(self.driver, 10).until(self.finish_loading)
        finally:
            self.driver.find_element_by_css_selector(self.searched_item).click()

        # set first amount
        first_amount = self.driver.find_element_by_id('transaction_transaction_account_lines_attributes_1_debit')
        first_amount.click()
        time.sleep(0.5)
        first_amount.clear()
        first_amount.send_keys(amount)
        self.driver.find_element_by_css_selector('thead.table-header').click()

    def insert_withdrawal_witholding(self, amount, unit, account_number):
        self.is_using_mc()
        self.driver.find_element_by_id('witholding_toggle').click()
        time.sleep(0.5)
        if unit != '%':
            self.driver.find_element_by_id('btn-toggle-value').click()
        else:
            pass
        witholding_value = self.driver.find_element_by_name('transaction[witholding_value]')
        witholding_value.click()
        time.sleep(0.5)
        witholding_value.clear()
        witholding_value.send_keys(amount)
        self.driver.find_element_by_id('s2id_witholding_account').click()
        WebDriverWait(self.driver, 10).until(self.finish_loading)
        if self.is_using_mc() is True:
            select2_witholding_account = 's2id_autogen16_search'
        else:
            select2_witholding_account = 's2id_autogen15_search'
        self.driver.find_element_by_id(select2_witholding_account).send_keys(account_number)
        try:
            WebDriverWait(self.driver, 10).until(self.finish_loading)
        finally:
            self.driver.find_element_by_css_selector(self.searched_item).click()

    def create_transaction(self):
        self.driver.find_element_by_css_selector('div.copyright.col-lg-8.col-md-8.col-sm-8').click()
        time.sleep(0.5)
        self.driver.find_element_by_id('create-button').click()
