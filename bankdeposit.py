from selenium import webdriver
from jurnal_navigations import Navigations
from deposit_helper import BankDepositHelper
from selenium.webdriver.chrome.options import Options

chromeOptions = Options()
chromeOptions.add_argument("--kiosk")
driver = webdriver.Chrome()
#driver = webdriver.Chrome(chrome_options=chromeOptions)
n = Navigations(driver)
bd = BankDepositHelper(driver)

n.open_newblueranger()
n.login_irene()

n.open_bankdeposit_index()

bd.create_new_bankdeposit()
bd.select_depositto('cash')
bd.select_payer('irene')
bd.set_transaction_date('01/01/2018')
bd.select_tags('tag1')
bd.tax_inclusive_toggle()
bd.scroll_page()
bd.select_account('inventory')
bd.add_description('automated testing')
bd.select_tax('ppn')
bd.add_amount('100000')
bd.add_memo('automated test irene')
bd.click_create_button()

#n.view_journal_entry()
#n.check_journal_entry()
#n.close_browser()
